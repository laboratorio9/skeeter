# Skeeter
EN:
Ultra-light web api to generate Documents in PDF format with origin in multiple formats.
Its architecture proposes a plug-in installation through the use of containers.
Do you need this solution?
Just use it!

ES:
Api web ultraliviana para generar Documentos en formato PDF con origen en múltiples formatos.
Su arquitectura propone una instalaccion plug-in mediante el uso de containers. 
¿Tu necesitas esta solucion?
¡Sólo úsalo!
 
Tabla de Contenidos:

{:toc}

## 1. Lenguaje de Programacion
Se seleccionó Java en función de su independencia de plataforma.
   
## 2. Decisiones de Diseño
Se pensó crear una solucion que sea totalmente desacoplada de cualquier otra funcionalidad que pueda existir en el ecosistema que la requiera.
Ademas, para aumentar su capacidad, se plantea la necesidad de que pueda escalar horizontalmente.
    
### Modos de operacion
1. Ejecucion JAR
- Generar jar mediante 
>mvn clean install
- Ejecutar mediante
>java -jar skeeter-0.0.1-SNAPSHOT.jar
- Recibe como parámetro al directorio que provee los templates HTML
>PARAMETERS_DIRECTORY_TEMPLATES, Cuyo valor default es "/var/container/skeeter/templates"
    
2. Docker
- Docker Build
>docker build -t skeeter-docker-image:latest .
- Ejecutar el Container montando un volumen sobre un directorio con permisos.
>docker run -t -p 8080:8080 -v /home/USER/skeeter/templates:/var/container/skeeter/templates --name skeeter-docker-container skeeter-docker-image:latest
- Se hace importante limitar la memoria y cpus disponibles en el container para dar un entorno de ejecucion performante.
### Almacenamiento de templates
Las mismas deberan estar disponibles en el directorio default, aquel que se indique por parameto o aquel que correspoda al volumen utilizado para la gestion de los containers 
### Web services
El servicio es ejecutado en el contexto /skeeter.
El puerto configurado es el 8080.
    
### No utilizamos Bases de Datos
Dada la intencion de tener una herramienta desacoplada de cualqueir otro servicio, se desalienta la implementacion de cualquier funcionalidad que requiera de utilizar bases de datos.
    
## 3. Pruebas de Carga
La herramienta elegida para la ejecucion de pruebas fue JMETER
Si bien el desarrollo soporta su ejecución independiente, para fines prácticos se decidió aislar el entorno en containers Docker.

### 3.1. Escenarios de Prueba
Se plantea la conversion de un único template a PDF utilizando el único endpoint de la solucion.
Se realizaran 4 juegos de prueba de 100, 500, 750 y 1000 solicitudes en un intervalo de 10 segundos.
Para las pruebas se utiliza una PC Dell Latitude 7480, Core i7, 16gb de RAM y Disco de Estado sólido.

#### 3.1.1. Escenario 1:
Para esta priueba inicial, se propone limitar el container a 1 CPU y 1GB de RAM

| CPU's | Uso CPU |Memoria | Uso Memoria |Muestra | Promedio | Min Respuesta (ms) | Max Respuesta (ms) | Desviacion | Error % | Rendimiento |
| ---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
|1 | 102% |1G| 377mb | 100/10seg  | 3002  | 149  | 5439  | 1383  | 0.00 % |  9.3 /sec |
|1 | 103% |1G| 418mb | 500/10seg  | 18064 | 1236 | 32084 | 8026  | 0.00 % | 13.1 /sec |
|1 | 102% |1G| 449mb | 750/10seg  | 17413 | 534  | 32000 | 9251  | 0.00 % | 19.6 /Sec |
|1 | 102% |1G| 498mb | 1000/10seg | 31846 | 628  | 57654 | 17122 | 0.00 % | 14.1 /Sec |

#### 3.1.2. Escenario 2:
Para el segundo escenario se propone aumentar el CPU a 2 núcleos, observando importantes mejoras en el rendimiento.

| CPU's | Uso CPU |Memoria | Uso Memoria |Muestra | Promedio | Min Respuesta (ms) | Max Respuesta (ms) | Desviacion | Error % | Rendimiento |
| ---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|---------|
|2 | 90%  |1G| 320mb | 100/10seg  |  94  |  32  | 184   | 80   | 0.00 % | 10.0 /sec |
|2 | 190% |1G| 300mb | 500/10seg  | 88   |  32  | 1086  | 78   | 0.00 % | 48.8 /sec |
|2 | 208% |1G| 394mb | 750/10seg  | 2487 |  40  | 6461  | 1409 | 0.00 % | 54.2 /sec |
|2 | 202% |1G| 536mb | 1000/10seg | 4585 |  137 | 10865 | 2690 | 0.00 % | 55.1 /sec |

#### 3.1.2. Escenario 3:
Para el tercer escenario se propone mantener las mismas condiciones del escenario 2 pero iterando 36 veces cada juego de pruebas para poder evaluar la respuesta de la solucion en un tiempo prolongado. 

### 3.2. Conclusiones:
El primer escenario es apto para condisiones de baja carga, la solucion se torna un poco lenta para responder las peticiones pero logra resolverlas en tiempos satisfactorios.
El segundo escenario, al incorporar un núcleo adicional, manteniendo un consumo de memoria RAM equivalente presenta notables mejoras en la capacidad de procesamiento de las solicitudes llegando a cuadriplicar el rendimiento.
Se recomienda realizar pruebas con 3 y 4 núcleos para poder determinar si hay aun mas mejoras de rendimiento en equivalente consumo de memoria o verificar su incremento.
El tercer escenario 

### 3.3. Leyenda
- Muestra: número de solicitudes enviadas
- Promedio: una media aritmética para todas las respuestas (suma de todos los tiempos/recuento)
- Tiempo de respuesta mínimo (ms)
- Tiempo máximo de respuesta (ms)
- Desviación: consulte artículo sobre desviación estándar
- Tasa de error: porcentaje de pruebas fallidas
- Rendimiento: cuántas solicitudes por segundo maneja su servidor. Más grande es mejor.
- Media Bytes: tamaño de respuesta promedio
## 4. Proximas funcionalidades
- Swagger
- Autenticacion mediante ssh keys
- Multithreading
