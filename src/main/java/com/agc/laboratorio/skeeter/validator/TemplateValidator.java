package com.agc.laboratorio.skeeter.validator;

import org.springframework.beans.factory.annotation.Value;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

public class TemplateValidator {
    @Value("${parameters.directory.templates}")
    private String templatesDirectory;

    private Set<String> templateFilenamesCache;

    public TemplateValidator (){
        templateFilenamesCache = new HashSet<>();
    }

    public Boolean validate(String templateName){
        if(templateFilenamesCache.contains(templateName)){
            return true;
        }else{
            this.updateCache();
            return templateFilenamesCache.contains(templateName);
        }

    }

    private void updateCache(){
        File folder = new File(templatesDirectory);
        File[] listOfFiles = folder.listFiles();


        if(templateFilenamesCache == null){
            templateFilenamesCache = new HashSet<>();
        }
        for (int i = 0; i < listOfFiles.length; i++) {
            templateFilenamesCache.add(listOfFiles[i].getName());
        }
    }
}
