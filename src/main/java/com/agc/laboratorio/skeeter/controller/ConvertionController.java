package com.agc.laboratorio.skeeter.controller;

import com.agc.laboratorio.skeeter.exception.TemplateNameDoesNotExistsException;
import com.agc.laboratorio.skeeter.service.ConvertionService;
import com.agc.laboratorio.skeeter.validator.TemplateValidator;
import com.itextpdf.text.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("/convertion")
public class ConvertionController {
    Logger logger = LoggerFactory.getLogger(ConvertionController.class);

    @Autowired
    ConvertionService convertionService;

    @Autowired
    TemplateValidator templateValidator;

    @GetMapping("/")
    public String download(){
        return "Hello! ";
    }

    @PostMapping("/template")
    public void generateUsingTemplate(HttpServletResponse response, @RequestParam Optional<String> templateName, @RequestBody Map<String, String> body) throws IOException, DocumentException {
        logger.info("New Request [URL: generate-using-template] - TemplateName: " + templateName + " Parameters:" + body);
        String receivedTemplateName = templateName.orElseGet(() -> "Template Name not provided");

        if(!templateValidator.validate(receivedTemplateName + ".html")) throw new TemplateNameDoesNotExistsException(receivedTemplateName);

        String htmlInvoice = convertionService.buildHtmlFromTemplate(receivedTemplateName, body);


        ByteArrayOutputStream bos = convertionService.generateVoucherDocumentBaos(htmlInvoice);
        byte[] pdfReport = bos.toByteArray();

        String mimeType =  "application/pdf";
        response.setContentType(mimeType);
        response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", "voucherLiquidacion.pdf"));

        response.setContentLength(pdfReport.length);

        ByteArrayInputStream inStream = new ByteArrayInputStream( pdfReport);

        FileCopyUtils.copy(inStream, response.getOutputStream());
    }
}
