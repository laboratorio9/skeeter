package com.agc.laboratorio.skeeter.service;

import com.itextpdf.text.DocumentException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

public interface ConvertionService {
    String buildHtmlFromTemplate(String templateName, Map<String, String> parameters);

    ByteArrayOutputStream generateVoucherDocumentBaos(String html) throws IOException, DocumentException;
}
