package com.agc.laboratorio.skeeter.service;

import com.itextpdf.text.DocumentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templateresolver.FileTemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Map;

@Service
public class ConvertionServiceImpl implements ConvertionService {
    Logger logger = LoggerFactory.getLogger(ConvertionServiceImpl.class);

    @Value("${parameters.directory.templates}")
    private String templatesDirectory;

    private Context resolveHtmlTemplateAttributesContext(Map<String, String> parameters) {
        Context context = new Context();
        parameters.entrySet().stream().forEach(entry ->{
            context.setVariable(entry.getKey(), entry.getValue());
        });
        return context;
    }

    @Override
    public String buildHtmlFromTemplate(String templateName, Map<String, String> parameters) {
        logger.info("Build Html from Template [template: " + templatesDirectory + templateName);
        TemplateEngine templateEngine = new TemplateEngine();
        FileTemplateResolver templateResolver = new FileTemplateResolver ();
        templateResolver.setPrefix(templatesDirectory);
        templateResolver.setSuffix(".html");
        templateResolver.setTemplateMode("HTML");
        templateResolver.setOrder(templateEngine.getTemplateResolvers().size());
        templateResolver.setCharacterEncoding("UTF-8");
        templateResolver.setCacheable(false);
        templateResolver.setCheckExistence(true);

        templateEngine.setTemplateResolver(templateResolver);

        return templateEngine.process(templateName, this.resolveHtmlTemplateAttributesContext(parameters));
    }

    @Override
    public ByteArrayOutputStream generateVoucherDocumentBaos(String html) throws IOException, DocumentException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(baos);
        baos.close();

        return baos;
    }

}
