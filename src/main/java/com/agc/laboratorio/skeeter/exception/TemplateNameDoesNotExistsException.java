package com.agc.laboratorio.skeeter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.NOT_FOUND)
public class TemplateNameDoesNotExistsException extends RuntimeException {
    public TemplateNameDoesNotExistsException() {
    }

    public TemplateNameDoesNotExistsException(String templateName) {
        super("Template not found for file name : " + templateName );
    }
}