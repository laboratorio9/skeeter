package com.agc.laboratorio.skeeter;

import com.agc.laboratorio.skeeter.validator.TemplateValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SkeeterContext {

    @Bean
    public TemplateValidator templateValidator() {
        return new TemplateValidator();
    }
}
