package com.agc.laboratorio.skeeter.web;

import com.agc.laboratorio.skeeter.exception.TemplateNameDoesNotExistsException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {
    private static final String SERVICE_NAME = "skeeter-conversor";

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse error = new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR, "Server Error", SERVICE_NAME, "SKT-ERR500", details);
        log.error("### Handling Unexpected Exception: ", ex);
        return new ResponseEntity<>(error, error.getStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> details = new ArrayList<>();
        for(ObjectError error : ex.getBindingResult().getAllErrors()) {
            details.add(error.getDefaultMessage());
        }
        ErrorResponse error = new ErrorResponse(HttpStatus.BAD_REQUEST,"Validation Failed", SERVICE_NAME, "SKT-ERR400",details);
        log.error("### Handling MethodArgumentNotValidException: ", ex);
        return new ResponseEntity(error, error.getStatus());
    }

    @ExceptionHandler(TemplateNameDoesNotExistsException.class)
    public final ResponseEntity<Object> handleTemplateNameDoesNotExistsException(TemplateNameDoesNotExistsException ex, WebRequest request) {
        List<String> details = new ArrayList<>();
        details.add(ex.getLocalizedMessage());
        ErrorResponse error = new ErrorResponse(HttpStatus.NOT_FOUND, "Record Not Found",SERVICE_NAME, "SKT-ERR204-1", details);
        log.error("### Handling TemplateNameDoesNotExistsException: ", ex);
        return new ResponseEntity(error, error.getStatus());
    }

}