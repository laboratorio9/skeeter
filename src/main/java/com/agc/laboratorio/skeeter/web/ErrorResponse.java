package com.agc.laboratorio.skeeter.web;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
public class ErrorResponse
{
    private HttpStatus status;

    private String error;

    private String service;

    private String code;

    private List<String> details;
}