package com.agc.laboratorio.skeeter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkeeterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkeeterApplication.class, args);
	}

}
