FROM maven:3-jdk-13-alpine AS builder
ARG MVN_REPO_USER
ARG MVN_REPO_PASS
WORKDIR /usr/src/Skeeter
COPY . /usr/src/Skeeter
RUN mvn clean install

FROM openjdk:13-alpine
#RUN addgroup -S spring && adduser -S spring -G spring
#USER spring:spring
RUN mkdir -p /var/container/skeeter/templates
EXPOSE 8080
COPY --from=builder /usr/src/Skeeter/target/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]